# A Rust Exercise to invoke C APIs from a Rust Container Application

Created by prateek.sahay@sentaca.com September 27, 2021



## Rationale for Rust


Rust is used by Disney Streaming Services for, among other use cases, the development of TestRunner. TestRunner is constructed as a flexible environment to invoke the DSS ADK player under test. Therefore, it is important that TestRunner invokes the C APIs exposed by an ADK developed player using FFI ( foreign-function interfaces).

Rust is a multi-paradigm, high-level, general-purpose programming language designed for performance and safety, especially safe concurrency. It solves pain points present in many other languages, providing a solid step forward with a limited number of downsides.


Rust provides the choice of storing data on the stack or on the heap and determines at compile time when memory is no longer needed and can be cleaned up. This allows efficient usage of memory as well as more performant memory access.


Without the need to have a garbage collector continuously running, Rust projects are well-suited to use libraries developed in other programming languages via foreign-function interfaces. This allows existing projects to replace performance-critical pieces with speedy Rust code without the memory safety risks inherent with other systems programming languages.


## Exercise Goals

This exercise show my approach to:

1. Construct a simple Rust container Application
2. Construct a mock (or stub) C Library that represents a video player interface
3. Create a CMake script to build  items #1 and #2 above from source
4. Create a video that shows the repository and the successful build and execution of the package

This project goes into further detail to demonstrate how the foreign-fuction interfaces work by taking a simple example of a mock video player. This is done by demonstrating three mock functions of a video player (“Play”, “Pause” and “Stop”) which are written in C.

In the TestRunner use case  the exposed media player APIs for a candidate ADK video player would be used.

The Rust application main.rs prompts a user to input a selection to either Play, Pause or Stop a stream presented to the mock player. Upon receiving the user input, the implementation calls the C functions for the requested function. The Rust container application terminates when the “Stop” option is selected.

The steps to build this project are outlined below. The source code should be referred to along with these steps to gain a better understanding of the process.



1. Created a Project directory using the “cargo new” command. For example: cargo new ffidemo



2. Inside the Project directory, make another directory (libffidemo)  just for the Mock Video Player, implemented in C (https://gitlab.com/prateek.sahay/ffi_demo/-/tree/main/libffidemo)


  1. In this sub-directory, create a .C file (https://gitlab.com/prateek.sahay/ffi_demo/-/blob/main/libffidemo/ffidemo.c) containing the C code we want to use in the project along with the *CMakeLists.txt* file (https://gitlab.com/prateek.sahay/ffi_demo/-/blob/main/libffidemo/CMakeLists.txt).


3. Create another folder (src) to add the Rust code. In this directory, create a rust file with a *.rs* extension (*main.rs*) (https://gitlab.com/prateek.sahay/ffi_demo/-/blob/main/src/main.rs).


4. In this file we need to link the C library used along with the C functions added. This can be done using the code below:

```
#[link(name = "ffidemo", kind = "static")]
extern "C" {
    fn Play() -> ();
    fn Pause() -> ();
    fn Stop() -> ();
}
```


5. Now create a *Cargo.toml* file (https://gitlab.com/prateek.sahay/ffi_demo/-/blob/main/Cargo.toml) and make sure to include CMake version under build dependencies. Cargo.toml is about describing your dependencies in a broad sense, and is written by you.


6. Now create a *build.rs* file (https://gitlab.com/prateek.sahay/ffi_demo/-/blob/main/build.rs) and *Cargo.lock* file (https://gitlab.com/prateek.sahay/ffi_demo/-/blob/main/Cargo.lock). Make sure to properly configure the *build.rs* file to be able to use CMake. Cargo.lock files contains exact information about your dependencies. It is maintained by Cargo and should not be manually edited.


7. Now go to the level of the project directory and use the command “cargo run” to build and run the project.


## Important Links:


Tip of GitLab Repository : https://gitlab.com/prateek.sahay/ffi_demo




Video (screen recording of executing the  project demonstration): https://gitlab.com/prateek.sahay/ffi_demo/-/blob/main/Demo_Video/C_From_Rust_Demo.mkv
